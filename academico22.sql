-- phpMyAdmin SQL Dump
-- version 5.3.0-dev
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.30.23
-- Tempo de geração: 11/10/2022 às 21:11
-- Versão do servidor: 8.0.18
-- Versão do PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `academico22`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `alunos`
--

CREATE TABLE `alunos` (
  `id` int(11) NOT NULL,
  `matricula` varchar(7) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telefone` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `alunos`
--

INSERT INTO `alunos` (`id`, `matricula`, `nome`, `cpf`, `email`, `telefone`) VALUES
(1, '656576', 'Bandon cedillo sanchez ', 'u6896778989', 'branmex23@gmail.com', '432235425'),
(2, '0066155', 'joan rico ortiz ', '2222222', 'ricoortiz@gmail.com', '2222222'),
(3, '234567', 'jona orihuela ', '11111111111', 'jona@gmail.com', '1111111'),
(11111127, '086473', 'Hugo Neymar do Cristiano de Messi', '27456428712', 'hglindo@gmail.com', '40028922');

-- --------------------------------------------------------

--
-- Estrutura para tabela `faltas`
--

CREATE TABLE `faltas` (
  `id` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `faltas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `faltas`
--

INSERT INTO `faltas` (`id`, `id_aluno`, `mes`, `faltas`) VALUES
(1, 666777, 6, 0),
(2, 1111122, 10, 8),
(3, 2, 10, 2),
(4, 86473, 3, 76),
(5, 66945, 9, 5),
(11111112, 11111112, 70, 9999),
(11111113, 66497, 11, 4),
(11111114, 11111123, 8, 24),
(11111115, 11111123, 8, 24),
(11111116, 222333, 0, 15),
(11111117, 555555, 10, 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `notas`
--

CREATE TABLE `notas` (
  `id` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `nota1` decimal(3,1) DEFAULT NULL,
  `nota2` decimal(3,1) DEFAULT NULL,
  `nota3` decimal(3,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `notas`
--

INSERT INTO `notas` (`id`, `id_aluno`, `nota1`, `nota2`, `nota3`) VALUES
(10, 1111112, '10.0', '10.0', '9.0'),
(11111112, 11111112, '99.0', '87.0', '98.0');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `faltas`
--
ALTER TABLE `faltas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `alunos`
--
ALTER TABLE `alunos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11111128;

--
-- AUTO_INCREMENT de tabela `faltas`
--
ALTER TABLE `faltas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11111118;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
